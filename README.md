# Microtext
A twitter-text-java port to Go. Enables you to extract and optionally autolink URL's in text.

by **Joris Kluivers**

- Read my weblog at [http://joris.kluivers.nl](http://joris.kluivers.nl).
- Follow [@kluivers on Twitter](http://twitter.com/kluivers).

## Features
Currently `microtext` supports:

- extracting URL's
- autolinking URL's in text
- shortening URL's for display

Mentions & hashtags are not implemented.

## Example

    import (
        "kluivers/microtext"
    )
    
    text = microtext.Autolink(text)

## What's in the name

The name `microtext` refers to the text processing in status updates for `microblogs`.

