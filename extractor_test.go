package microtext

import (
    "testing"
)

func TestExtract1(t *testing.T) {
    text := "Text with google.com"
    
    entities := Extract(text)
    
    if len(entities) != 1 {
        t.Error("Wrong number of entities found")
    }
    
    entity := entities[0]
    
    if entity.URL != "http://google.com" {
        t.Error("Incorrect URL found: expected http://google.com")
    }
}

func TestExtractCCSimplePath(t *testing.T) {
    text := "Airwaves promo page - http://tarento.nl/airwaves/"
    
    entities := Extract(text)
    
    if (len(entities) != 1) {
        t.Error("Wrong number of entities found")
    }
    
    entity := entities[0]
    
    if entity.URL != "http://tarento.nl/airwaves/" {
        t.Errorf("Found incorrect URL. Found '%s', expected '%s'", entity.URL, "http://tarento.nl/airwaves/")
    }
}

func TestExtractCCExtendedPath(t *testing.T) {
    text := "Airwaves blog post - http://joris.kluivers.nl/blog/introducing-airwaves"
    
    entities := Extract(text)
    
    if (len(entities) != 1) {
        t.Error("Wrong number of entities found")
    }
    
    entity := entities[0]
    
    if entity.URL != "http://joris.kluivers.nl/blog/introducing-airwaves" {
        t.Errorf("Found incorrect URL. Found '%s', expected '%s'", entity.URL, "http://joris.kluivers.nl/blog/introducing-airwaves")
    }
}