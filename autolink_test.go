package microtext

import (
    "fmt"
)

func ExampleAutolinkURLWithoutHTTP() {
    urls := []string{
        "google.com",
        "joris.kluivers.nl",
        "kluivers.nl",
    }
    
    for _, url := range urls {
        fmt.Println(Autolink(url))
    }
    
    // Output:
    // <a href="http://google.com">google.com</a>
    // <a href="http://joris.kluivers.nl">joris.kluivers.nl</a>
    // <a href="http://kluivers.nl">kluivers.nl</a>
}

