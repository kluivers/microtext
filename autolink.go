package microtext

import (
	"fmt"
	"bytes"
)

func LinkToURL(entity Entity, text string, buffer *bytes.Buffer) {
	LinkToURLMaxLen(entity, text, 0, buffer)
}

func LinkToURLMaxLen(entity Entity, text string, maxLen int, buffer *bytes.Buffer) {
	if maxLen < 1 || len(entity.Text) <= maxLen {
		buffer.WriteString(fmt.Sprintf("<a href=\"%s\">%s</a>", entity.URL, entity.Text))
		return
	}

	buffer.WriteString(fmt.Sprintf("<a href=\"%s\">%s...</a>", entity.URL, entity.Text[:maxLen]))
}

func AutolinkEntities(text string, entities []Entity) string {
	return AutolinkEntitiesMaxLen(text, entities, 0)
}

func AutolinkEntitiesMaxLen(text string, entities []Entity, maxLen int) string {
	var beginIndex int = 0
	var buffer bytes.Buffer

	for _, entity := range entities {
		buffer.WriteString(text[beginIndex : entity.Start])
	
		LinkToURLMaxLen(entity, text, maxLen, &buffer)
		
		beginIndex = entity.End
	}
	
	buffer.WriteString(text[beginIndex:])

	return buffer.String()
}

func Autolink(text string) string {
	return AutolinkMaxLen(text, 0)
}

func AutolinkMaxLen(text string, maxLen int) string {
	entities := Extract(text)

	return AutolinkEntitiesMaxLen(text, entities, maxLen)
}